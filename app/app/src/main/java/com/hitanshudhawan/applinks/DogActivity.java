package com.hitanshudhawan.applinks;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class DogActivity extends AppCompatActivity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog);

        textView = findViewById(R.id.text_view);

        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();

        if (appLinkAction != null && appLinkAction.equals(Intent.ACTION_VIEW) && appLinkData != null) {
            String id = appLinkData.getLastPathSegment();
            textView.setText(id);
        }

    }
}
